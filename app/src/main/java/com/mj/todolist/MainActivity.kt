package com.mj.todolist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MyCustomDialog.InputListener{


    val TAG = "MainActivity"

    override fun sendInput(name: String, desc: String, editFlag: Boolean) {
        Log.d("INTERFACE_1", name +" "+desc)
        if (editFlag) { //edit entry
            itemList.set(itemPositionEdit, Item(name, desc))
            itemAdapter?.notifyDataSetChanged()
        }
        else{ //add new entry
            itemList.add(Item(name, desc))
        }
    }

    //variables
    private var itemList: ArrayList<Item> = arrayListOf()
    private var itemAdapter: ItemAdapter? = null
    private var itemPositionEdit: Int= 0 //to store index to be edited

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(my_toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false) //remove appbar title

        generateData() //fill in list with dummy data

        myRecyclerView.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        itemAdapter = ItemAdapter(itemList)
        myRecyclerView.adapter = itemAdapter

        //Handles reyclerview itemclickListener
        itemAdapter?.customClickListener { position, menuId ->
            when(menuId){
                R.id.menu_delete -> {
                   itemList.removeAt(position)
                    itemAdapter?.notifyDataSetChanged()
                }
                R.id.menu_edit -> {
                    itemPositionEdit = position //store the index to be edited
                    val myCustomDialog =
                        MyCustomDialog(itemList[position].itemName,
                            itemList[position].itemDescription)
                    myCustomDialog.show(supportFragmentManager, "MyCustomDialog(name, desc)")
                }
                else -> {

                }
            }
        }

        fab.setOnClickListener {
           /* toast("FAB Triggered!!!")*/
            val myCustomDialog = MyCustomDialog()
            myCustomDialog.show(supportFragmentManager, "MyCustomDialog()")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.toolbar_menu, menu)

        return true
    }



    private fun generateData(){
        for (i in 1..5){
            itemList?.add(Item("Model "+i.toString(),
                "Description "+i.toString()))
        }
    }

    fun toast(text: String) {
        Toast.makeText(applicationContext,text,Toast.LENGTH_SHORT).show()
    }

}




