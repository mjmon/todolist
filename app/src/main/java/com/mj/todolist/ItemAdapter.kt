package com.mj.todolist

import android.content.Context
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

class ItemAdapter(private var items: ArrayList<Item>):
        RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    //Variables
    private var mContext: Context? = null
    private var onItemClick: ((position: Int, menuId: Int) -> Unit)? = null

    fun customClickListener(customListener: (position: Int, menuId: Int) -> Unit){
        onItemClick = customListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemAdapter.ViewHolder {
        mContext = parent.context
        val itemView = LayoutInflater.from(mContext)
            .inflate(R.layout.item_list_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ItemAdapter.ViewHolder, position: Int) {
        holder.txtItemName.text = items[position].itemName
        holder.txtItemDescription.text = items[position].itemDescription

        holder.itemView.setOnClickListener {
            Log.d("onbind", position.toString())

            var popup = PopupMenu(mContext!!, holder.itemView, Gravity.CENTER)
            popup.menuInflater.inflate(R.menu.recyclerview_popupmenu, popup.menu)
            popup.show()
            popup.setOnMenuItemClickListener {
                when(it.itemId){
                    R.id.menu_edit ->{
                        toast("Edit Selected")
                        onItemClick?.invoke(position, R.id.menu_edit)
                        true
                    }
                    R.id.menu_delete ->{
                        toast("Delete Selected")
                        onItemClick?.invoke(position, R.id.menu_delete)
                        true
                    }
                    else -> {
                        true
                    }
                }
            }
        }

        /*
        holder.itemView.setOnClickListener {
            Log.d("onbind", position.toString())

            var popup = PopupMenu(mContext!!, holder.itemView, Gravity.CENTER)
            popup.menuInflater.inflate(R.menu.recyclerview_popupmenu, popup.menu)
            popup.show()
            popup.setOnMenuItemClickListener {
               when(it.itemId){
                   R.id.menu_edit ->{
                       toast("Edit Selected")
                       *//*toast(R.id.menu_edit.toString())*//*
                       true
                   }
                   R.id.menu_delete ->{
                       toast("Delete Selected")
                       *//*toast(R.id.menu_delete.toString())*//*
                       true
                   }
                   else -> {
                       true
                   }
               }
            }
        }*/
    }

    private fun toast(text: String){
        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show()
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row){


        var txtItemName: TextView
        var txtItemDescription: TextView

        init {
            this.txtItemName = row.findViewById(R.id.txtItemName)
            this.txtItemDescription = row.findViewById(R.id.txtItemDescription)
        }
    }
}