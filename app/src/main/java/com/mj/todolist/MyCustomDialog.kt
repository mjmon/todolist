package com.mj.todolist

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MyCustomDialog() : DialogFragment() {

    interface InputListener{
        fun sendInput(name: String, description: String, editFlag: Boolean)
    }

    @SuppressLint("ValidFragment")
    constructor(name: String, description: String) : this() {
        this.name = name
        this.desc = description
    }

    private val TAG = "MyCustomDialog"
    //fragment widgets
    private var txtName: EditText? = null
    private var txtDescription: EditText? = null
    private var btnCancel: Button? = null
    private var btnsubmit: Button? = null
    //variables
    var myInputListener: InputListener? = null
    private var name: String? = null
    private var desc: String?=null
    private var editFlag: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")

        val view = inflater.inflate(R.layout.popupdialog,container, false)


        txtName = view.findViewById(R.id.txtDialogName)
        txtDescription = view.findViewById(R.id.txtDialogDescription)
        btnCancel = view.findViewById(R.id.btnDialogCancel)
        btnsubmit = view.findViewById(R.id.btnDialogSubmit)

        //set value if editmenu was selected for editing
        if(name != null && desc !=null){
            txtName?.setText(name)
            txtDescription?.setText(desc)
            editFlag = true
        }

        btnCancel?.setOnClickListener {
                dialog.dismiss()
            }

        btnsubmit?.setOnClickListener {
                name = txtName?.text.toString()
                desc = txtDescription?.text.toString()

                if (txtName?.text.toString().trim().isNotEmpty() &&
                    txtDescription?.text.toString().trim().isNotEmpty()){
                    myInputListener?.sendInput(name!!, desc!!, editFlag)
                    dialog.dismiss()
                }else{
                    toast("Please Fill In All Fields")
                }
            }

        return view
    }

    //attach Listener
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is InputListener){
            myInputListener = context
        }
        else{
            throw RuntimeException(context!!.toString()+"must implement InputListener")
        }
    }
    //Detach Listener
    override fun onDetach() {
        super.onDetach()
        myInputListener = null
    }

    private fun toast(text: String){
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }
}